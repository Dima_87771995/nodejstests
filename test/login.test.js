const { browser } = require('protractor');
const loginPage = require('../pages/login.page')
const inventoryPage = require('../pages/inventory.page')
const configData = require('../config')
const testData = require('../constants')
const inventory = require('../inventory')

const expect = require('chai').expect

describe('Login suit', function () {
    
    it('login test using standard user', async function () {
        
        await loginPage.get();

        await loginPage.logoIsDisplayed();
        await loginPage.botIsDisplayed();
        expect(await loginPage.getErrorText()).to.be.a('string');

        await loginPage.setName(configData.standardUser);
        await loginPage.setPassword(configData.password);
        await loginPage.clickLoginBtn();

        await inventoryPage.logoIsDisplayed();
        await inventoryPage.titleIsDisplayed();

        expect(await inventoryPage.getTitleText()).to.equal(testData.title);
        expect(await inventoryPage.getInventoryNames().count()).to.equal(6);

        for (let i = 0; i < await inventoryPage.getInventoryNames().count();  i += 1) {
            expect(await inventoryPage.getInventoryNames().get(i).getText()).to.equal(inventory[i].itemName);
        }

        for (let i = 0; i < await inventoryPage.getInventoryPrices().count(); i += 1) {
            expect(await inventoryPage.getInventoryPrices().get(i).getText()).to.equal(inventory[i].price);
        }

    });

    it('login test using locked out user', async function () {
        
        await loginPage.get();

        await loginPage.logoIsDisplayed();
        await loginPage.botIsDisplayed();
        expect(await loginPage.getErrorText()).to.be.a('string');

        await loginPage.setName(configData.lockedUser);
        await loginPage.setPassword(configData.password);
        await loginPage.clickLoginBtn();
        expect(await loginPage.getErrorText()).to.equal(testData.lockedOutText);

    });
})