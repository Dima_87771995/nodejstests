const { element } = require("protractor")
const { waitForElementVisible } = require('../commons')

var LoginPage = function() {
    var username = element(by.css('[name="user-name"]'));
    var password = element(by.css('[type="password"]'));
    var loginBtn = element(by.css('[type="submit"]'));
    var logotype = element(by.css('div.login_logo'));
    var botPicture = element(by.css('div.bot_column'));
    var errorMessage = element(by.css('div.error-message-container'));

    
    this.get = async function() {
      await browser.get('https://www.saucedemo.com/');
    };
  
    this.setName = async function(name) {
      await username.sendKeys(name);
    };

    this.setPassword = async function(passw) {
        await password.sendKeys(passw);
    };

    this.clickLoginBtn = async function() {
        await loginBtn.click();
    };

    this.logoIsDisplayed = async function() {
      await logotype.isPresent();
    };

    this.botIsDisplayed = async function() {
      await botPicture.isPresent();
    };

    this.getErrorText = function() {
      waitForElementVisible(errorMessage);
      return errorMessage.getText();
    };
  };

  module.exports = new LoginPage();
