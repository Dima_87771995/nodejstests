const { element } = require('protractor');
const { waitForElementVisible } = require('../commons')

var InventoryPage = function() {
    var logotype = element(by.css('div.app_logo'));
    var title = element(by.css('span.title'));
    var inventoryItemName = $$('div.inventory_item_name');
    var inventoryItemPrice = $$('div.inventory_item_price');

    this.logoIsDisplayed = async function() {
        await waitForElementVisible(logotype);
    };

    this.titleIsDisplayed = async function() {
        await waitForElementVisible(title);
    };

    this.getTitleText = function() {
        return title.getText();
    };

    this.getInventoryNames = function() {
        return inventoryItemName;
    };

    this.getInventoryPrices = function() {
        return inventoryItemPrice;
    };

};

module.exports = new InventoryPage();