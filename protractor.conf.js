const conf = {
    specs: ['./test/**/*.test.js'],
    framework: 'mocha',
    mochaOpts: {
      timeout: 30 * 1000
    },
    onPrepare() {
      browser.waitForAngularEnabled(false)
      browser.manage().window().maximize()
    },
    SELENIUM_PROMISE_MANAGER: false
  };
  
  exports.config = conf